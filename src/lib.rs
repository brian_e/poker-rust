#![deny(warnings)]

extern crate rand;

use std::collections::HashMap;
use std::fmt;
use std::iter::FromIterator;

const HAND_SIZE: usize = 5;
const SUIT_COUNT: usize = 4;

const POSSIBLE_STRAIGHTS: [[u8; HAND_SIZE]; 10] = [[14, 13, 12, 11, 10],
                                                   [13, 12, 11, 10, 9],
                                                   [12, 11, 10, 9, 8],
                                                   [11, 10, 9, 8, 7],
                                                   [10, 9, 8, 7, 6],
                                                   [9, 8, 7, 6, 5],
                                                   [8, 7, 6, 5, 4],
                                                   [7, 6, 5, 4, 3],
                                                   [6, 5, 4, 3, 2],
                                                   [5, 4, 3, 2, 14]];

#[derive(Default, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Hand {
    straight_flush: Option<[u8; HAND_SIZE]>,
    quads: Option<[u8; HAND_SIZE]>,
    boat: Option<[u8; HAND_SIZE]>,
    flush: Option<[u8; HAND_SIZE]>,
    straight: Option<[u8; HAND_SIZE]>,
    trips: Option<[u8; HAND_SIZE]>,
    pair_count: u8,
    played_ranks: [u8; HAND_SIZE],
}

impl Hand {
    pub fn new(cards: &str) -> Hand {
        assert!(cards.len() >= HAND_SIZE * 2);
        assert!(cards.len() < HAND_SIZE * 4);
        assert!(cards.len() % 2 == 0);

        let (all_ranks, all_ranks_of_flush_suit) = all_ranks(cards);

        if let Some(all_ranks_of_flush_suit) = all_ranks_of_flush_suit.clone() {
            if let Some(straight_flush) = straight(&all_ranks_of_flush_suit) {
                return Hand { straight_flush: Some(straight_flush), ..Default::default() };
            }
        }

        let (played_ranks, dup) = played_ranks(&all_ranks);

        if dup[4] > 0 {
            return Hand { quads: Some(played_ranks), ..Default::default() };
        }

        if dup[3] > 0 && dup[3] + dup[2] > 1 {
            return Hand { boat: Some(played_ranks), ..Default::default() };
        }

        if let Some(all_flush) = all_ranks_of_flush_suit {
            let mut played_flush: [u8; HAND_SIZE] = [0; HAND_SIZE];
            played_flush.clone_from_slice(&all_flush[0..HAND_SIZE]);
            return Hand { flush: Some(played_ranks), ..Default::default() };
        }

        if let Some(straight) = straight(&all_ranks) {
            return Hand { straight: Some(straight), ..Default::default() };
        }

        if dup[3] > 0 {
            return Hand { trips: Some(played_ranks), ..Default::default() };
        }

        return Hand {
            pair_count: dup[2],
            played_ranks: played_ranks,
            ..Default::default()
        };
    }
}

impl fmt::Display for Hand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if let Some(ranks) = self.straight_flush {
            return write!(f, "straight flush {:?}", to_rank_chars(ranks));
        }
        if let Some(ranks) = self.quads {
            return write!(f, ".........quads {:?}", to_rank_chars(ranks));
        }
        if let Some(ranks) = self.boat {
            return write!(f, "..........boat {:?}", to_rank_chars(ranks));
        }
        if let Some(ranks) = self.flush {
            return write!(f, ".........flush {:?}", to_rank_chars(ranks));
        }
        if let Some(ranks) = self.straight {
            return write!(f, "......straight {:?}", to_rank_chars(ranks));
        }
        if let Some(ranks) = self.trips {
            return write!(f, ".........trips {:?}", to_rank_chars(ranks));
        }
        if self.pair_count > 1 {
            return write!(f, "......two pair {:?}", to_rank_chars(self.played_ranks));
        }
        if self.pair_count > 0 {
            return write!(f, "..........pair {:?}", to_rank_chars(self.played_ranks));
        }
        return write!(f, ".....high card {:?}", to_rank_chars(self.played_ranks));
    }
}

fn to_rank_chars(ranks: [u8; HAND_SIZE]) -> Vec<char> {
    let mut rank_chars = Vec::new();
    for rank in &ranks {
        rank_chars.push("--23456789TJQKA".chars().nth(*rank as usize).unwrap());
    }
    return rank_chars;
}

fn all_ranks(cards: &str) -> (Vec<u8>, Option<Vec<u8>>) {
    let cards = Vec::from_iter(cards.chars());
    let mut all_ranks = Vec::new();
    let mut ranks_by_suit = HashMap::new();
    for card in cards.chunks(2) {
        let rank = match card[0].to_digit(10) {
            Some(rank) => rank as u8,
            None => 10 + "TJQKA".find(card[0]).unwrap() as u8,
        };
        all_ranks.push(rank);
        ranks_by_suit.entry(card[1]).or_insert(Vec::new()).push(rank);
    }
    all_ranks.sort_by(|a, b| b.cmp(a));
    let mut all_ranks_of_flush_suit = None;
    for ranks_of_suit in ranks_by_suit.values() {
        if ranks_of_suit.len() >= HAND_SIZE {
            let mut r = ranks_of_suit.clone();
            r.sort_by(|a, b| b.cmp(a));
            all_ranks_of_flush_suit = Some(r);
            break;
        }
    }
    return (all_ranks, all_ranks_of_flush_suit);
}

fn played_ranks(ranks: &Vec<u8>) -> ([u8; HAND_SIZE], [u8; SUIT_COUNT + 1]) {
    let mut rank_groups: Vec<Vec<u8>> = Vec::new();
    let mut group: Vec<u8> = Vec::new();
    for rank in ranks {
        if group.len() == 0 || group[0] == *rank {
            group.push(*rank);
        } else {
            rank_groups.push(group);
            group = vec![*rank];
        }
    }
    rank_groups.push(group);  // push the final group
    rank_groups.sort_by(|a, b| (b.len(), b).cmp(&(a.len(), a)));
    let played_ranks = find_played_ranks(&rank_groups);
    let mut dup_counts: [u8; SUIT_COUNT + 1] = [0; SUIT_COUNT + 1];
    for group in rank_groups {
        dup_counts[group.len()] += 1;
    }
    return (played_ranks, dup_counts);
}

fn straight(ranks: &Vec<u8>) -> Option<[u8; HAND_SIZE]> {
    let mut ranks = ranks.clone();
    ranks.dedup();
    if ranks[0] == 14 {
        ranks.push(14);
    }
    for straight in &POSSIBLE_STRAIGHTS {
        if let Ok(i) = ranks.binary_search_by(|p| straight[0].cmp(p)) {
            if ranks.len() - i >= HAND_SIZE && ranks[i..HAND_SIZE + i] == *straight {
                return Some(*straight);
            }
        }
    }
    return None;
}

fn find_played_ranks(rank_groups: &Vec<Vec<u8>>) -> [u8; HAND_SIZE] {
    let mut played_ranks: [u8; HAND_SIZE] = [0; HAND_SIZE];
    let mut i: usize = 0;
    for group in rank_groups {
        for rank in group {
            played_ranks[i] = *rank;
            if i > HAND_SIZE - 2 {
                return played_ranks;
            }
            i += 1;
        }
    }
    panic!("cannot determine played ranks from {:?}", rank_groups);
}

#[cfg(test)]
mod tests {

    use Hand;

    #[test]
    fn test_high_card_7() {
        let ace_high = Hand::new("KsQsTs9dAs2c3d");
        let ace_high_b = Hand::new("AdKdQdTd9s5c3c");
        assert_eq!(ace_high, ace_high_b);
        let king_high = Hand::new("KdQdJd9s8d2c3c");
        assert!(ace_high > king_high);
        let ace_high_q = Hand::new("AdQdJd9s8s2c3d");
        assert!(ace_high > ace_high_q);
        assert!(Hand::new("2c3d4dTh9h7c5d") > Hand::new("2c3d4dTh8h7c5d"))
    }

    #[test]
    fn test_high_card_5() {
        let ace_high = Hand::new("KsQsTs9dAs");
        let ace_high_b = Hand::new("AdKdQdTd9s");
        assert_eq!(ace_high, ace_high_b);
        let king_high = Hand::new("KdQdJd9s8d");
        assert!(ace_high > king_high);
        let ace_high_q = Hand::new("AdQdJd9s8s");
        assert!(ace_high > ace_high_q);
        assert!(Hand::new("2c3c4c6c7h") > Hand::new("2h3h4h5h7c"))
    }

    #[test]
    fn test_one_pair_7() {
        let pair_2s = Hand::new("8s2sQs3d9d2c7s");
        assert_eq!(pair_2s, Hand::new("8s2sQs4d9d2c7s"));
        let ace_high = Hand::new("2c7sAcKdQcJc9h");
        assert!(pair_2s > ace_high);
        let pair_3s = Hand::new("8d3d2c7s3d2h9h");
        assert!(pair_3s > pair_2s);
        assert!(pair_2s > Hand::new("8d2dJd2c7s3s9s"));
    }

    #[test]
    fn test_one_pair_5() {
        let pair_2s = Hand::new("8s2sQs2d9d");
        let ace_high = Hand::new("AcKcQcJc9h");
        assert!(pair_2s > ace_high);
        let pair_3s = Hand::new("8d3d3d2h9h");
        assert!(pair_3s > pair_2s);
        assert!(pair_2s > Hand::new("8d2dJd2s9s"));
    }

    #[test]
    fn test_two_pair_7() {
        let fours_over_2s = Hand::new("4s2s4d5h2h6cJc");
        assert!(fours_over_2s > Hand::new("AsKsQsJs9d6cJc"));
        assert!(fours_over_2s > Hand::new("8dAdAd2c9h6cJc"));
        assert!(fours_over_2s > Hand::new("3s2s4d3h2h6cJc"));
        assert_eq!(fours_over_2s, Hand::new("4s2s4d3h2h6cJc"));
        assert!(fours_over_2s > Hand::new("4s2s4d3h2h6cTc"));
    }

    #[test]
    fn test_two_pair_5() {
        let fours_over_2s = Hand::new("4s2s4d5h2h");
        assert!(fours_over_2s > Hand::new("AsKsQsJs9d"));
        assert!(fours_over_2s > Hand::new("8dAdAd2c9h"));
        assert!(fours_over_2s > Hand::new("3s2s4d3h2h"));
        assert!(fours_over_2s > Hand::new("4s2s4d3h2h"));
    }

    #[test]
    fn test_trips_7() {
        let trip_3s = Hand::new("3s3c3d5h4h6cJc");
        assert!(trip_3s > Hand::new("AsKsQsJs9d6cJc"));
        assert!(trip_3s > Hand::new("8dAdAd2c9h6cJc"));
        assert!(trip_3s > Hand::new("4s2s4d5h2h6cJc"));
        assert_eq!(trip_3s, Hand::new("3s3c3d5h2h6cJc"));
        assert!(trip_3s > Hand::new("3s3c3d5h2h6cTc"));
    }

    #[test]
    fn test_trips_5() {
        let trip_3s = Hand::new("3s3c3d5h4h");
        assert!(trip_3s > Hand::new("AsKsQsJs9d"));
        assert!(trip_3s > Hand::new("8dAdAd2c9h"));
        assert!(trip_3s > Hand::new("4s2s4d5h2h"));
        assert!(trip_3s > Hand::new("3s3c3d5h2h"));
    }

    #[test]
    fn test_straight_7() {
        let wheel = Hand::new("As2d3d4d5d3cJc");
        assert!(wheel > Hand::new("AsKsQsJs9d6cJc"));
        assert!(wheel > Hand::new("8dAdAd2c9h6cJc"));
        assert!(wheel > Hand::new("4s2s4d5h2h6cJc"));
        assert!(wheel > Hand::new("3s3c3d5h4h6cJc"));
        assert!(Hand::new("2d3d6d4c5d6cJc") > wheel);
        assert!(Hand::new("2d3c4d5c6d7c8d") > Hand::new("Td3c4d5c6d7cTd"));
    }

    #[test]
    fn test_straight_5() {
        let wheel = Hand::new("As2d3d4d5d");
        assert!(wheel > Hand::new("AsKsQsJs9d"));
        assert!(wheel > Hand::new("8dAdAd2c9h"));
        assert!(wheel > Hand::new("4s2s4d5h2h"));
        assert!(wheel > Hand::new("3s3c3d5h4h"));
        assert!(Hand::new("2d3d6d4c5d") > wheel);
    }

    #[test]
    fn test_flush_7() {
        let flush = Hand::new("TdJd2d4d7dAcJc");
        assert!(flush > Hand::new("AsKsQsJs9d6cJc"));
        assert!(flush > Hand::new("8dAdAd2c9h6cJc"));
        assert!(flush > Hand::new("4s2s4d5h2h6cJc"));
        assert!(flush > Hand::new("3s3c3d5h4h6cJc"));
        assert!(flush > Hand::new("2d3d6c4d5d6cJc"));
        assert!(Hand::new("QsKs2sTs5s6cJc") > flush);
    }

    #[test]
    fn test_flush_5() {
        let flush = Hand::new("TdJd2d4d7d");
        assert!(flush > Hand::new("AsKsQsJs9d"));
        assert!(flush > Hand::new("8dAdAd2c9h"));
        assert!(flush > Hand::new("4s2s4d5h2h"));
        assert!(flush > Hand::new("3s3c3d5h4h"));
        assert!(flush > Hand::new("2d3d6c4d5d"));
        assert!(Hand::new("AsKs2sTs5s") > flush);
    }

    #[test]
    fn test_boat_7() {
        let boat = Hand::new("6h7c9c6d9h6cJc");
        assert!(boat > Hand::new("AsKsQsJs9d6cJc"));
        assert!(boat > Hand::new("8dAdAd2c9h6cJc"));
        assert!(boat > Hand::new("4s2s4d5h2h6cJc"));
        assert!(boat > Hand::new("3s3c3d5h4h6cJc"));
        assert!(boat > Hand::new("2d3d6c4d5d6cJc"));
        assert!(boat > Hand::new("AsKs2sTs5s6cJc"));
        assert!(Hand::new("2d2c8c8d8h6cJc") > boat);
        assert_eq!(Hand::new("AcAdAhKsKdTd2d"), Hand::new("AcAdAhKsKd6c4c"))
    }

    #[test]
    fn test_boat_5() {
        let boat = Hand::new("6h6c9c6d9h");
        assert!(boat > Hand::new("AsKsQsJs9d"));
        assert!(boat > Hand::new("8dAdAd2c9h"));
        assert!(boat > Hand::new("4s2s4d5h2h"));
        assert!(boat > Hand::new("3s3c3d5h4h"));
        assert!(boat > Hand::new("2d3d6c4d5d"));
        assert!(boat > Hand::new("AsKs2sTs5s"));
        assert!(Hand::new("2d2c8c8d8h") > boat);
    }

    #[test]
    fn test_quads_7() {
        let quads = Hand::new("6h6c9c6d6s7cJc");
        assert!(quads > Hand::new("AsKsQsJs9d6cJc"));
        assert!(quads > Hand::new("8dAdAd2c9h6cJc"));
        assert!(quads > Hand::new("4s2s4d5h2h6cJc"));
        assert!(quads > Hand::new("3s3c3d5h4h6cJc"));
        assert!(quads > Hand::new("2d3d6c4d5d6cJc"));
        assert!(quads > Hand::new("AsKs2sTs5s6cJc"));
        assert!(quads > Hand::new("2d2c8c8d8h6cJc"));
        assert_eq!(Hand::new("6h6cTc6d6s7cJc"), quads);
        assert!(Hand::new("6h6cTc6d6s7cQc") > quads);
        assert!(Hand::new("7h7c9c7d7s6cJc") > quads);
    }

    #[test]
    fn test_quads_5() {
        let quads = Hand::new("6h6c9c6d6s");
        assert!(quads > Hand::new("AsKsQsJs9d"));
        assert!(quads > Hand::new("8dAdAd2c9h"));
        assert!(quads > Hand::new("4s2s4d5h2h"));
        assert!(quads > Hand::new("3s3c3d5h4h"));
        assert!(quads > Hand::new("2d3d6c4d5d"));
        assert!(quads > Hand::new("AsKs2sTs5s"));
        assert!(quads > Hand::new("2d2c8c8d8h"));
        assert!(Hand::new("6h6cTc6d6s") > quads);
        assert!(Hand::new("7h7c9c7d7s") > quads);
    }

    #[test]
    fn test_straight_flush_7() {
        let straight_flush = Hand::new("6d9d8d5d7d6cJc");
        assert!(straight_flush > Hand::new("AsKsQsJs9d6cJc"));
        assert!(straight_flush > Hand::new("8dAdAd2c9h6cJc"));
        assert!(straight_flush > Hand::new("4s2s4d5h2h6cJc"));
        assert!(straight_flush > Hand::new("3s3c3d5h4h6cJc"));
        assert!(straight_flush > Hand::new("2d3d6c4d5d6cJc"));
        assert!(straight_flush > Hand::new("AsKs2sTs5s6cJc"));
        assert!(straight_flush > Hand::new("2d2c8c8d8h6cJc"));
        assert!(straight_flush > Hand::new("6h6cTc6d6s7cJc"));
        assert!(Hand::new("AdKdQdTdJd6cJc") > straight_flush);
    }

    #[test]
    fn test_straight_flush_5() {
        let straight_flush = Hand::new("6d9d8d5d7d");
        assert!(straight_flush > Hand::new("AsKsQsJs9d"));
        assert!(straight_flush > Hand::new("8dAdAd2c9h"));
        assert!(straight_flush > Hand::new("4s2s4d5h2h"));
        assert!(straight_flush > Hand::new("3s3c3d5h4h"));
        assert!(straight_flush > Hand::new("2d3d6c4d5d"));
        assert!(straight_flush > Hand::new("AsKs2sTs5s"));
        assert!(straight_flush > Hand::new("2d2c8c8d8h"));
        assert!(straight_flush > Hand::new("6h6cTc6d6s"));
        assert!(Hand::new("AdKdQdTdJd") > straight_flush);
    }

}

pub mod bench {

    use rand;
    use rand::Rng;
    use rand::distributions::{IndependentSample, Range};
    use Hand;

    const NUM_PLAYERS: u8 = 10;
    const NUM_HOLE_CARDS: u8 = 2;
    const NUM_BOARD_CARDS: u8 = 5;

    const RANKS: &'static str = "AKQJT98765432";
    const SUITS: &'static str = "cshd";

    pub struct Bench {
        full_deck: Box<Vec<String>>,
        rng: Box<Rng>,
    }

    impl Bench {
        pub fn new() -> Bench {
            let mut full_deck = Vec::new();
            for suit in SUITS.chars() {
                for rank in RANKS.chars() {
                    let mut card = String::new();
                    card.push(rank);
                    card.push(suit);
                    full_deck.push(card);
                }
            }
            return Bench {
                full_deck: Box::new(full_deck),
                rng: Box::new(rand::thread_rng()),
            };
        }

        pub fn run(&mut self) -> (String, Vec<(Hand, String)>) {
            let mut hands = Vec::new();
            let (board_cards, players) = self.gen();
            for (cards, hole_cards) in players {
                hands.push((Hand::new(&cards), hole_cards));
            }
            hands.sort_by(|a, b| b.cmp(a));
            return (board_cards, hands);
        }

        pub fn print(&mut self) {
            let (_, players) = self.gen();
            println!("&[");
            for (cards, _) in players {
                println!("{:?},", cards);
            }
            println!("],");
        }

        fn gen(&mut self) -> (String, Vec<(String, String)>) {
            let mut deck = self.full_deck.clone();
            let mut board_cards = String::new();
            for _ in 0..NUM_BOARD_CARDS {
                let index = Range::new(0, deck.len()).ind_sample(&mut self.rng);
                board_cards.push_str(&deck.remove(index));
            }
            let mut players = Vec::new();
            for _ in 0..NUM_PLAYERS {
                let mut cards = board_cards.clone();
                let mut hole_cards = String::new();
                for _ in 0..NUM_HOLE_CARDS {
                    let index = Range::new(0, deck.len()).ind_sample(&mut self.rng);
                    let card = &deck.remove(index);
                    cards.push_str(card);
                    hole_cards.push_str(card);
                }
                players.push((cards, hole_cards));
            }
            return (board_cards, players);
        }
    }

}
