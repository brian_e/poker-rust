#![deny(warnings)]

extern crate poker;

use std::env;
use poker::bench::Bench;

const DEFAULT_NUM_RUNS: u32 = 1000;

fn main() {
    let num_runs = match env::args().nth(1) {
        Some(num_runs) => num_runs.parse::<u32>().unwrap(),
        None => DEFAULT_NUM_RUNS,
    };
    let mut bench = Bench::new();
    for _ in 0..num_runs {
        let (board_cards, sorted_hands) = bench.run();
        let mut last_hand = None;
        println!("Board: {}", board_cards);
        for i in 0..sorted_hands.len() {
            let (ref hand, ref hole_cards) = sorted_hands[i];
            if i > 0 && hand == last_hand.unwrap() {
                println!("    {} {}", hole_cards, hand);
            } else {
                println!("{:>2}. {} {}", i + 1, hole_cards, hand);
            }
            last_hand = Some(hand);
        }
    }
}
