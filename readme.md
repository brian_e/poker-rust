# Poker hand evaluator written in Rust

## How to run the program

Running the program simulates dealing a game of Texas Hold'em where all ten
players stay in for the showdown. 5 board cards are shared by all the players.
The program displays the hands in order of strength.

	$ cargo run 1

	Board: 4sKdQd8h4h
	 1. Qh4c ..........boat ['4', '4', '4', 'Q', 'Q']
	 2. TdKs ......two pair ['K', 'K', '4', '4', 'Q']
	    Kc3h ......two pair ['K', 'K', '4', '4', 'Q']
	 4. Qc6s ......two pair ['Q', 'Q', '4', '4', 'K']
	    7hQs ......two pair ['Q', 'Q', '4', '4', 'K']
	 6. TcTs ......two pair ['T', 'T', '4', '4', 'K']
	 7. 2dAd ..........pair ['4', '4', 'A', 'K', 'Q']
	 8. 9dJs ..........pair ['4', '4', 'K', 'Q', 'J']
	    7dJd ..........pair ['4', '4', 'K', 'Q', 'J']
	10. 9h5c ..........pair ['4', '4', 'K', 'Q', '9']


## How to test the program

	$ cargo test

	running 18 tests
	test tests::test_boat_5 ... ok
	test tests::test_flush_5 ... ok
	test tests::test_flush_7 ... ok
	test tests::test_one_pair_5 ... ok
	test tests::test_one_pair_7 ... ok
	test tests::test_high_card_7 ... ok
	test tests::test_boat_7 ... ok
	test tests::test_high_card_5 ... ok
	test tests::test_quads_5 ... ok
	test tests::test_straight_5 ... ok
	test tests::test_straight_7 ... ok
	test tests::test_quads_7 ... ok
	test tests::test_straight_flush_5 ... ok
	test tests::test_trips_5 ... ok
	test tests::test_straight_flush_7 ... ok
	test tests::test_two_pair_5 ... ok
	test tests::test_trips_7 ... ok
	test tests::test_two_pair_7 ... ok
	test result: ok. 18 passed; 0 failed; 0 ignored; 0 measured


## How to benchmark the program

	$ cargo bench

	running 1 test
	test tests::bench ... bench:      15,040 ns/iter (+/- 1,917)
	test result: ok. 0 passed; 0 failed; 0 ignored; 1 measured


## How to profile the program

https://github.com/pegasos1/cargo-profiler

	$ cargo profiler callgrind --bin target/debug/examples/profiling |egrep '(Total Instructions|poker)'

	Total Instructions...476,553,252
	10,504,441 (2.2%) ???:poker::played_ranks
	10,033,875 (2.1%) ???:poker::all_ranks
	7,018,024 (1.5%) ???:poker::straight
	5,738,098 (1.2%) ???:_..poker..Hand..as..core..cmp..Ord..::cmp
	4,599,162 (1.0%) ???:poker::straight::_....closure....
	4,429,459 (0.9%) ???:poker::find_played_ranks
	3,130,000 (0.7%) ???:poker::to_rank_chars
	2,315,881 (0.5%) ???:poker::Hand::new
	1,730,772 (0.4%) ???:poker::all_ranks::_....closure....
	1,617,316 (0.3%) ???:_..poker..Hand..as..core..fmt..Display..::fmt
	1,390,532 (0.3%) ???:poker::played_ranks::_....closure....
	900,000 (0.2%) ???:_..poker..Hand..as..core..default..Default..::default


	$ cargo profiler cachegrind --bin target/debug/examples/profiling |egrep '(Total|I1mr|poker)'

	Total Memory Accesses...2,208,710,785
	Total L1 I-Cache Misses...7,376,590 (0%)
	Total LL I-Cache Misses...1,011 (0%)
	Total L1 D-Cache Misses...84,699 (0%)
	Total LL D-Cache Misses...2,354 (0%)
	 Ir  I1mr ILmr  Dr  D1mr DLmr  Dw  D1mw DLmw
	0.01 0.04 0.03 0.01 0.00 0.00 0.01 0.00 0.01 lib.rs:poker::played_ranks
	0.01 0.04 0.03 0.01 0.00 0.00 0.01 0.00 0.00 lib.rs:poker::all_ranks
	0.01 0.02 0.01 0.00 0.00 0.00 0.01 0.00 0.00 lib.rs:poker::straight
	0.00 0.00 0.02 0.00 0.00 0.00 0.00 0.00 0.00 lib.rs:_poker..Handascore..cmp..Ord::cmp
	0.00 0.01 0.01 0.00 0.00 0.00 0.00 0.00 0.00 lib.rs:poker::find_played_ranks
	0.00 0.00 0.01 0.00 0.00 0.00 0.00 0.00 0.00 lib.rs:poker::to_rank_chars
	0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 ???:poker::straight::_closure
	0.00 0.04 0.06 0.00 0.00 0.00 0.00 0.00 0.00 lib.rs:poker::Hand::new
	0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 lib.rs:poker::straight::_closure
	0.00 0.01 0.08 0.00 0.00 0.00 0.00 0.00 0.00 lib.rs:_poker..Handascore..fmt..Display::fmt
