#![deny(warnings)]
#![feature(test)]

extern crate test;
extern crate poker;

#[cfg(test)]
mod tests {

    use test::Bencher;
    use poker::bench::Bench;

    #[bench]
    fn bench(bencher: &mut Bencher) {
        let mut bench = Bench::new();
        bencher.iter(|| bench.run());
    }
}
